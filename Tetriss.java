package com.señoritopi.tetris;

import javax.swing.*;
import java.awt.*;

public class Tetriss extends JFrame {
    JLabel barraEstado;

    public Tetriss(){
        barraEstado = new JLabel(" 0");
        add(barraEstado, BorderLayout.SOUTH);
        Tablero tablero = new Tablero(this);
        add(tablero);
        tablero.start();

        setSize(200, 400);
        setTitle("Tetris");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public JLabel getBarraEstado(){
        return barraEstado;
    }

    public static void main(String[] args){
        Tetriss juego = new Tetriss();
        juego.setLocationRelativeTo(null);
        juego.setVisible(true);
    }
}