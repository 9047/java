import java.util.Scanner;
public class Menor {
	public static void main(String[] args) {
		Scanner teclado=new Scanner(System.in);
		int n1, n2, n3;
		if (args.length<1) {
			System.out.println("Introduce el valor de numero 1: ");
			n1=teclado.nextInt();
			System.out.println("Introduce el valor de numero 2: ");
			n2=teclado.nextInt();
			System.out.println("Introduce el valor de numero 3: ");
			n3=teclado.nextInt();
		}else {
			n1=Integer.parseInt(args[0]);
			n2=Integer.parseInt(args[1]);
			n3=Integer.parseInt(args[2]);
		}

		if (n1>n2){
			System.out.println("El numero " +n1+ " Es Mayor que " +n2);		
		}else {
			System.out.println("El numero " +n1+ " Es Menor que " +n2);
		}
		if (n1<n3){
			System.out.println("El numero " +n1+ " Es Menor que " +n3);
		}else{
			System.out.println("El numero " +n1+ " Es Mayor que " +n3);
		}
		if (n2<n3){
			System.out.println("El numero " +n2+ " Es Menor que " +n3);
		}else{
			System.out.println("El numero " +n2+ " Es Mayor que " +n3);
		}

	}
}