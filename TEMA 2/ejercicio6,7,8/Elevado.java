//8.- Programa que reciba dos números y calcule la potencia del primero elevado al segundo
import java.util.Scanner;
public class Elevado{
	public static void main(String[] args) {
	int n1;
	int n2;
	Scanner teclado=new Scanner(System.in);
	if (args.length<1) {
			System.out.println("Introduce el valor de numero 1: ");
			n1=teclado.nextInt();
			System.out.println("Introduce el valor de numero 2: ");
			n2=teclado.nextInt();
		}else {
			n1=Integer.parseInt(args[0]);
			n2=Integer.parseInt(args[1]);
		}
		System.out.println(n1+" ** "+n2+" = "  +(int)Math.pow(n1,n2));
	}	
}