/*7.- Programa que reciba un número e imprima todos los números primos comprendidos entre 1 
y ese número (un número es primo si y sólo si es divisible por 1 y él mismo)*/
import java.util.Scanner;
public class Primo{
	public static void main(String[] args) {
		Scanner teclado=new Scanner(System.in);
		int numero;
		int contador=1;
		if (args.length<1) {
			System.out.println("Introduce el valor de numero 1: ");
			numero=teclado.nextInt();
		}else {
			numero=Integer.parseInt(args[0]);
		}
		while (contador<=numero) {
			if (esPrimo(contador))
			System.out.println(contador);
			contador++;	
		}	
	}
	public static boolean esPrimo(int conta){
		boolean primo=true;
		for(int i=2;i<conta;i++){
			if(conta%i==0) {
				primo=false;
			}
		}			
		return primo;
	}
}