//6.- Programa que reciba un número, n,  y muestre las tablas de multiplicar del 2 hasta n
import java.util.Scanner;
public class Multiplica {
	public static void main(String[] args) {
		Scanner teclado=new Scanner(System.in);
		int n, i;
		if (args.length<1) {
			System.out.println("Introduce el valor de numero 1: ");
			n=teclado.nextInt();
		}else {
			n=Integer.parseInt(args[0]);
		}
		for (i=1 ; i<=n; i++) {
			//System.out.println( n+ " * " + i + " = " +n*i);
			//imprimeTabla(i);
			System.out.println("Tabla del " +i);
			System.out.println("----------------");
			for(int tabla=1; tabla<=10;tabla++){
				System.out.println( i+ " * " + tabla + " = " +i*tabla);
	
			}
			System.out.println("----------------");			
		}
	}
}