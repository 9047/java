public class Prueba {
	public static boolean compruebaNota(String nota) {
		if (nota.equalsIgnoreCase("Matricula") || nota.equalsIgnoreCase("Sobresaliente") || nota.equalsIgnoreCase("Notable Alto") 
			|| nota.equalsIgnoreCase("Notable Bajo") || nota.equalsIgnoreCase("Bien") || nota.equalsIgnoreCase("Suficiente") || nota.equalsIgnoreCase("Suspenso")) {
			return true;
		} else {
			return false;
		}

	}
	public static void main(String args[]) {
		//String nota;
	//	if (args.length<1) {
			//System.out.println("Necesito un parámetro que sea una nota en el rango [1,10]");
			//System.exit(1);
		//}
		String nota=args[0].toLowerCase();
		int notaNum=0;
		if (compruebaNota(nota)) {
			switch(nota) {
				case "matricula": notaNum=10; break;
				case  "sobresaliente": notaNum=9; break;
				case  "notable alto": notaNum=8; break;
				case  "notable bajo":  notaNum=7; break;
				case  "bien":  notaNum=6; break;
				case "suficiente":  notaNum=5; break;
				case "suspenso":  notaNum=4; break;
				default: System.out.println("Nota incorrecta");
			}
		} else {
			System.out.println("La nota es Incorrecta");
			System.exit(1);
		}
		//terminación del break
		System.out.println(notaNum);
	}
}

