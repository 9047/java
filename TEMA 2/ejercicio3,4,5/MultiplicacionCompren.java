import java.util.Scanner;
public class MultiplicacionCompren{
	public static void main(String[] args) {
		Scanner teclado=new Scanner(System.in);
		int n,i=1, resultado=1;
		if (args.length<1) {
			System.out.println("Introduce el  numero comprendido: ");
			n=teclado.nextInt();
		}else{
			n=Integer.parseInt(args[0]);
		}
		boolean negativo;
		if (n<0) {
			negativo=true;
			n=-n;
		}else{
			negativo=false;
		}
		while (i<=n){
			resultado=resultado*i;
			i++;
		}
		if (negativo && n%2==1){
			System.out.println("La multiplicacion 1 hasta "+n+" es: "+(-resultado));

		}else{
			System.out.println("La multiplicacion 1 hasta "+n+" es: "+resultado);
		}
	}
}