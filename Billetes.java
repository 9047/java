import java.util.Scanner;
public class Billetes
{
    public static void main(String[]args)
    {
        int c;
        Scanner tec = new Scanner(System.in);
        System.out.println("Indica la cantidad en euros");
        c = tec.nextInt();
        int doscientos = c / 200;
        c = c - 200 * doscientos;
        int veinte = c / 20;
        c = c - 20 * veinte;
        int diez = c / 10;
        c = c - 10 * diez;
        System.out.println(doscientos + " Billetes de 200");
        System.out.println(veinte + " Billetes de 20");
        System.out.println(diez + " Billetes de 10");
        System.out.println("Sobran " + c + " euros");
    }
}