public class Joven {
	String nombre;
	String apellido1;
	String apellido2;
	int edad;
	int nivelDeEstudios;
	double ingresos;

	public Joven(String nombre,String apellido1,String apellido2, int edad, int nivelDeEstudios, double ingresos) {
		//short edad;
		//byte nivelDeEstudios;
		//double ingresos
		this.nombre=nombre;
		this.apellido1=apellido1;
		this.apellido2=apellido2;
		this.edad=edad;
		this.nivelDeEstudios=nivelDeEstudios;
		this.ingresos=ingresos;
	}

	public void establecerJasp(){
		this.jasp=(this.edad<=28 && this.nivelDeEstudios>3 && this.ingresos>28000);
	}
	public static void main(String[] args) {
		Joven p=new Joven("Pepito", "Lopez", "Perez", 18,4,28000.65);
		Joven q=new Joven("Lola", "Lopez", "Perez", 15,2,0.0);
		p.establecerJasp();
		q.establecerJasp();
		System.out.println(p.jasp);
		//System.out.println(p);
		//System.out.println(q);
	}
}